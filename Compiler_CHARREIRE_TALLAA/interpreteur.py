import sys

registres = [0 for _ in range(16)]
ram       = [0 for _ in range(256)]
pointer   = 0

def loadFile(fileName) :

	f = open(fileName, "r")

	if f.mode != 'r' :
		print("Cannot open the file {}".format(fileName))
		exit(-2)
	
	fl = f.readlines()

	limit = int("70", 16)

	if (len(fl) > limit) :
		print("Cannot load the instructions : the file is too long.")
		exit(-3)

	k = 0
	for i in fl :
		ram[k] = int(i[0:], 16)
		k = k+2

	f.close()

	return 2*len(fl)


def executeInstruction(instr, pointer) :
	sinstr = '{:08x}'.format(instr)
	OP = int(sinstr[0:2], 16)
	A  = int(sinstr[2:4], 16)
	B  = int(sinstr[4:6], 16)
	C  = int(sinstr[6:8], 16)

	if (OP == 1) : #ADD
		registres[A] = registres[B] + registres[C]

	if (OP == 2) : #MUL
		registres[A] = registres[B] * registres[C]

	if (OP == 3) : #SOU
		registres[A] = registres[B] - registres[C]

	if (OP == 4) : #DIV
		registres[A] = registres[B] // registres[C]

	if (OP == 5) : #COP
		registres[A] = registres[B]

	if (OP == 6) : #AFC
		registres[A] = B

	if (OP == 7) : #LOAD
		registres[A] = ram[B]

	if (OP == 8) : #STORE
		ram[A] = registres[B]

	if (OP == 9) : #EQU
		if registres[B] == registres[C] : 
			registres[A] = 1 
		else :
			registres[A] = 0

	if (OP == 10) : #INF
		if registres[B] < registres[C] :
			registres[A] = 1 
		else :
			registres[A] = 0

	if (OP == 11) : #INFE
		if registres[B] <= registres[C] :
			registres[A] = 1 
		else :
			registres[A] = 0

	if (OP == 12) : #SUP
		if registres[B] > registres[C] :
			registres[A] = 1 
		else :
			registres[A] = 0

	if (OP == 13) : #SUPE
		if registres[B] >= registres[C] :
			registres[A] = 1 
		else :
			registres[A] = 0

	if (OP == 14) : #JMP
		pointer = A-2

	if (OP == 15) : #JMPC
		if registres[B] == 0 :
			pointer = A-2

	return pointer +2
#==============================================================================#
# MAIN                                                                         #
#==============================================================================#

if len(sys.argv) < 3 :
	top = loadFile(sys.argv[1])
else :
	print("Usage : python3 interpreteur.py [filename]")
	exit(-1);

while (pointer < top) :
	print("0x{:02x} | 0x{:08x}".format(pointer, ram[pointer]))
	pointer = executeInstruction(ram[pointer], pointer)
	print(registres)


