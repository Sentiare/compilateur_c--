#ifndef TABLE_H
#define TABLE_H

#include <stdbool.h>

//****************************************************************************//
// DEFINE                                                                     //
//****************************************************************************//

#define REF_ADR_MEM 0x70
#define SIZE_OF_INT 1
#define SIZE_OF_CONST 1


//****************************************************************************//
// ENUMERATIONS                                                               //
//****************************************************************************//
enum type { INT, CONST };
typedef enum type typeEnum;

enum temps { TMP, RGL };
typedef enum temps tmpEnum;


//****************************************************************************//
// STRUCTURES                                                                 //
//****************************************************************************//  

struct symbole {
	char *   name;
	int      address;
	typeEnum type;
	int      depth;
	int      tmp;
	struct symbole * next;
};
typedef struct symbole S_symbole;

typedef struct {
	S_symbole * root;
	int         length;
	int         current_address;
	int         current_depth;
} S_symbolTab;


//****************************************************************************//
// PRIMITIVES                                                                 //
//****************************************************************************//

int put(char * n, typeEnum t, tmpEnum tmp);
S_symbole * find(char * name);
void init(void);
int getAndIncAdr(typeEnum e);
int  getDepth(void);
void incDepth(void);
bool decDepth(void);
void printTab(void);

int popOneTMP(void);
bool popAllTmp(void);

S_symbolTab * GLOBAL_TAB;


#endif
