%{
  #include <stdio.h>
	#include "y.tab.h"
	void yyerror (char * s);
	int yylex(void);
%}

%%
"main"                  {ECHO;  return tMAIN;}
"if"                    {ECHO;  return tIF;}
"else"                  {ECHO;  return tELSE;}
"while"                 {ECHO;  return tWHILE;}
"("                     {ECHO;  return tPO;}
")"                     {ECHO;  return tPF;}
"{"			                {ECHO;  return tAO;}
"}"                     {ECHO;  return tAF;}
"const"                 {ECHO;  return tCONST;}
"int"                   {ECHO;  return tINT;}
"+"                     {ECHO;  return tADD;}
"-"                     {ECHO;  return tSUB;}
"*"                     {ECHO;  return tMUL;}
"/"                     {ECHO;  return tDIV;}
"=="                    {ECHO;  return tEGALE;}
"<="                    {ECHO;  return tINFEG;}
">="                    {ECHO;  return tSUPEG;}
">"                     {ECHO;  return tSUP;}
"<"                     {ECHO;  return tINF;}
"="                     {ECHO;  return tEQU;}
","                     {ECHO;  return tCOMMA;}
"\t"                    {ECHO;}
" "                     {ECHO;}
"\n"                    {ECHO;}
";"                     {ECHO;  return tSC;}
"printf"                {ECHO;  return tPRINTF;}
[a-zA-Z][a-zA-Z0-9_]*   {ECHO; yylval.str = strdup(yytext);  return tID;}
[0-9]+(e[0-9]*)?        {ECHO; float f; sscanf(yytext, "%f", &f);  yylval.nb = f; return tNB;}


