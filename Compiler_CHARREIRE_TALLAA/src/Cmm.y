%{
  #include <stdio.h>
	#include <stdlib.h>
	#include "table.h"
	#include "instr.h"
	void yyerror(char * s);
	int yylex(void);
%}

%union {
	char *str;
	int nb;
	struct instruction * instr;
}

%token tMAIN tPO tPF tAO tAF tCONST tINT tSUB tMUL tDIV tEQU tCOMMA tSC tPRINTF tIF tELSE
%token tEGALE tINF tSUP tINFEG tSUPEG
%token <str> tID
%token <nb> tNB
%token <nb> tWHILE
%type<instr> While
%type<nb> Formule
%type<nb> Add Sub Mul Div Inf Sup Infe Supe Parentheses
%start S

%left tCOMMA
%right tEQU
%left  tADD tSUB
%left  tMUL tDIV
%left  tEGALE tINF tSUP tINFEG tSUPEG

%%
S           : tINT tMAIN tPO tPF tAO {incDepth();} Body {	printTab();} tAF {decDepth();} {printf("\nFile Accepted\n");};
Body        : Lignes;

Lignes        : Ligne | Ligne Lignes;
Ligne         : Type_de_Ligne tSC | If | While;

If : IfCore {getIfCell()->instr->arg1 = val(COMPSTRUCT->instr_table->currentAdr);
             popIfCell();}
   | IfCore ElseCore {getIfCell()->instr->arg1 = val(COMPSTRUCT->instr_table->currentAdr);
                      popIfCell();};

IfCore : tIF Parentheses {putInstr(LOAD, reg(0),  val(popOneTMP()), empty());
                          putInstr(JMPC, empty(), reg(0),           empty());
											    putIfCell(COMPSTRUCT->instr_table->last);} 
         tAO {incDepth();} Body tAF {decDepth();};


ElseCore : tELSE {instruction * storage = getIfCell()->instr;
                  popIfCell();
                  putInstr(JMP, empty(), empty(), empty());
					        putIfCell(COMPSTRUCT->instr_table->last);
                  storage->arg1 = val(COMPSTRUCT->instr_table->currentAdr);} 
                 tAO {incDepth();} 
                     Body tAF {decDepth();};

While : tWHILE {$1 = COMPSTRUCT->instr_table->currentAdr;} Parentheses {putInstr(LOAD, reg(0),  val(popOneTMP()), empty());
                                                                        putInstr(JMPC, val(COMPSTRUCT->instr_table->currentAdr + 2*SIZE_OF_INSTRUCTION), reg(0),           empty());
                                                                        $<instr>$ = COMPSTRUCT->instr_table->last;} 
        tAO Body tAF {putInstr(JMP, val($1), empty(), empty());
                      $<instr>4->arg1 = val(COMPSTRUCT->instr_table->currentAdr);};


Type_de_Ligne : Declaration | Affectation | Printf;

Declaration : tINT ID_Ints | tCONST ID_Consts;

ID_Ints : ID_Int 
				| ID_Int tCOMMA ID_Ints;

ID_Int : tID {put($1, INT, RGL);}  //Declaration
			 | tID {put($1, INT, RGL);} tEQU Formule {putInstr(LOAD,  reg(0),                 val(popOneTMP()), empty());
                                                putInstr(STORE, val(find($1)->address), reg(0),           empty());}; //Affectation

ID_Consts : ID_Const 
				  | ID_Const tCOMMA ID_Consts;

ID_Const : tID {put($1, CONST, RGL);} tEQU Formule {putInstr(LOAD,  reg(0),                 val(popOneTMP()), empty());
                                                    putInstr(STORE, val(find($1)->address), reg(0),           empty());}; //Affectation

Affectation : tID tEQU Formule {putInstr(LOAD,  reg(0),                 val(popOneTMP()), empty());
                                putInstr(STORE, val(find($1)->address), reg(0),           empty());};

Formule     : tID {putInstr(LOAD,  reg(0),  val(find($1)->address), empty());
                   $$ = put("tmp", INT, TMP);
                   putInstr(STORE, val($$), reg(0), empty());}

            | tNB {putInstr(AFC,   reg(0), val($1), empty());
                   $$ = put("val", INT, TMP);
                   putInstr(STORE, val($$), reg(0), empty());}

            | Parentheses
						| Add
						| Sub
						| Mul
						| Div
						| Inf
            | Sup
            | Infe
						| Supe;


Parentheses : tPO Formule tPF {$$ = $2;};

Add : Formule tADD Formule {putInstr(LOAD,  reg(0),                    val(popOneTMP()), empty());
                            putInstr(LOAD,  reg(1),                    val(popOneTMP()), empty());
                            putInstr(ADD,   reg(0),                    reg(0),           reg(1));
                            putInstr(STORE, val(put("add", INT, TMP)), reg(0),           empty()); };

Sub : Formule tSUB Formule {putInstr(LOAD,  reg(0),                    val(popOneTMP()), empty());
                            putInstr(LOAD,  reg(1),                    val(popOneTMP()), empty());
                            putInstr(SOU,   reg(0),                    reg(1),           reg(0));
                            putInstr(STORE, val(put("sub", INT, TMP)), reg(0),           empty());};

Mul : Formule tMUL Formule {putInstr(LOAD,  reg(0),                    val(popOneTMP()), empty());
                            putInstr(LOAD,  reg(1),                    val(popOneTMP()), empty());
                            putInstr(MUL,   reg(0),                    reg(1),           reg(0));
                            putInstr(STORE, val(put("mul", INT, TMP)), reg(0),           empty());};

Div : Formule tDIV Formule {putInstr(LOAD,  reg(0),                    val(popOneTMP()), empty());
                            putInstr(LOAD,  reg(1),                    val(popOneTMP()), empty());
                            putInstr(DIV,   reg(0),                    reg(1),           reg(0));
                            putInstr(STORE, val(put("div", INT, TMP)), reg(0),           empty());};

Inf : Formule tINF Formule {putInstr(LOAD,  reg(0),                    val(popOneTMP()), empty());
                            putInstr(LOAD,  reg(1),                    val(popOneTMP()), empty());
                            putInstr(INF,   reg(0),                    reg(1),           reg(0));
                            putInstr(STORE, val(put("inf", INT, TMP)), reg(0),           empty());};

Sup : Formule tSUP Formule {putInstr(LOAD,  reg(0),                    val(popOneTMP()), empty());
                            putInstr(LOAD,  reg(1),                    val(popOneTMP()), empty());
                            putInstr(SUP,   reg(0),                    reg(1),           reg(0));
                            putInstr(STORE, val(put("sup", INT, TMP)), reg(0),           empty());};

Infe : Formule tINFEG Formule {putInstr(LOAD,  reg(0),                    val(popOneTMP()), empty());
                            putInstr(LOAD,  reg(1),                    val(popOneTMP()), empty());
                            putInstr(INFE,   reg(0),                    reg(1),           reg(0));
                            putInstr(STORE, val(put("infe", INT, TMP)), reg(0),           empty());};

Supe : Formule tSUPEG Formule {putInstr(LOAD,  reg(0),                    val(popOneTMP()), empty());
                            putInstr(LOAD,  reg(1),                    val(popOneTMP()), empty());
                            putInstr(SUPE,   reg(0),                    reg(1),           reg(0));
                            putInstr(STORE, val(put("supe", INT, TMP)), reg(0),           empty());};






Printf      : tPRINTF tPO Formule tPF {putInstr(LOAD,  reg(0), val(popOneTMP()), empty());
                                       putInstr(PRINT, reg(0), empty(),          empty());};                                       

%%

void yyerror(char * s) {
	printf("%s\n", s);
}

int main(int argc, char *argv[]) {
	char* name = "out.asm";

	if ((argc != 1)&&(argc != 2)) {
		printf("Usage : ./CmmCompiler [output file] < {input file}\n");
		exit(EXIT_FAILURE);	
	}	

	if (argc == 2) {
		name = argv[1];
	}

	initCompStruct();
	init();
	yyparse();
	makeASM();
	writeASM(name);
	return 0;
}


