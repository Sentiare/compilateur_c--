#ifndef INSTR_H
#define INSTR_H

//****************************************************************************//
// DEFINE                                                                     //
//****************************************************************************//

#define MAX_NB_REG 16
#define REF_ADR_INST 0x00
#define SIZE_OF_INSTRUCTION 2

//****************************************************************************//
// ENUMERATION AND MINOR STRUCTURE                                            //
//****************************************************************************//

typedef enum {
	ADD,   MUL,   SOU, 
	DIV,   COP,   AFC, 
	LOAD,  STORE, EQU, 
	INF,   INFE,  SUP, 
	SUPE,  JMP,   JMPC,
	PRINT,
} enumCMD;

typedef struct {
	char * string;
} ARG;

//****************************************************************************//
// COMPILER STRUCT                                                            //
//****************************************************************************//

struct instruction {
	int address;
	enumCMD  cmd;
	ARG arg1;
	ARG arg2;
  ARG arg3;
	struct instruction * next;
};
typedef struct instruction instruction;

typedef struct {
	instruction * root;
	instruction * last;
	int length;
	int currentAdr;
} instructionTable;

struct ifCell {
	instruction * instr;
	int argIndex;
	struct ifCell * next;
};
typedef struct ifCell ifCell;

typedef struct {
	ifCell * top;
	int size;
} ifStack;

typedef struct {
	ifStack * if_stack;
	instructionTable * instr_table;
} compStruct;

//GLOBAL :
compStruct * COMPSTRUCT;

//****************************************************************************//
// PRIMITIVES                                                                 //
//****************************************************************************//

char * cmdToString(enumCMD cmd);

void initCompStruct(void);

instruction * putInstr(enumCMD cmd, ARG arg1, ARG arg2, ARG arg3);
ifCell * putIfCell(instruction * instr);
void popIfCell(void);
ifCell * getIfCell(void);
int  getAndIncInstrAdr(void);

ARG val(int v);
ARG reg(int r);
ARG empty(void);

void makeASM(void);
void writeASM(char* filename);

#endif
