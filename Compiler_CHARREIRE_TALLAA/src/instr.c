#include "instr.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

//****************************************************************************//
// GLOBAL VALUES                                                              //
//****************************************************************************//

char EMPTY_STRING[] = "";

char * cmdString[] =   { "ADD  ", "MUL  ", "SOU  ", 	
                         "DIV  ", "COP  ", "AFC  ",
                         "LOAD ", "STORE", "EQU  ", 	
                         "INF  ", "INFE ", "SUP  ", 
                         "SUPE ", "JMP  ", "JMPC ",
												 "PRINT" };

char * cmdToString(enumCMD cmd) {
	return cmdString[cmd];
}

//****************************************************************************//
// FUNCTIONS                                                                  //
//****************************************************************************//

/* Initialise la structure globale permettant la compilation.
 * A executer en début de programme.
 */

void initCompStruct() {	
	//malloc
	compStruct * newCompStruct = malloc(sizeof(compStruct));
	instructionTable * newInstrTable = malloc(sizeof(instructionTable));
	ifStack * newIfStack = malloc(sizeof(ifStack));

	//construction
	newCompStruct->if_stack                = newIfStack;
	newCompStruct->instr_table             = newInstrTable;
	newCompStruct->if_stack->top           = NULL;
	newCompStruct->if_stack->size          = 0;
	newCompStruct->instr_table->root       = NULL;
	newCompStruct->instr_table->last       = NULL;
	newCompStruct->instr_table->length     = 0;
	newCompStruct->instr_table->currentAdr = REF_ADR_INST;
	
	//Affectation
	COMPSTRUCT = newCompStruct;
}

/* val, reg, empty permettent de remplire les champs arguments des instructions.
 */

ARG val(int v) {
	ARG a;
	a.string = malloc(5*sizeof(char));
	if (v > 127) {
			sprintf(a.string, "%x", 127);
	} else if (v < 16) {
			sprintf(a.string, "0%x", v);
	} else if (v < -128) {
			sprintf(a.string, "%x", -127);
	} else {
			sprintf(a.string, "%x", v);
	}


	return a;
}

ARG reg(int r) {
	ARG a;
	if ((r >= 0)&&(r < MAX_NB_REG)) {
		char * regIndex = malloc(2*sizeof(char));
		sprintf(regIndex, "%x", r);
		a.string = regIndex;
	}	else {
		exit(-1);
	}
	return a;
}

ARG empty(void) {
	ARG a;
	a.string = EMPTY_STRING;
	return a;
}

/* Construit une instruction et l'ajoute à la structure de compilation.
 */
instruction * putInstr(enumCMD cmd, ARG arg1, ARG arg2, ARG arg3) {
	instruction * inst = malloc(sizeof(instruction));
	inst->address  = getAndIncInstrAdr();
	inst->cmd      = cmd;
	inst->arg1     = arg1;
	inst->arg2     = arg2;
	inst->arg3     = arg3;
	inst->next     = NULL;

	if (COMPSTRUCT->instr_table->length == 0) {
		COMPSTRUCT->instr_table->root = inst;
	} else {
		COMPSTRUCT->instr_table->last->next = inst;
	}
	COMPSTRUCT->instr_table->last = inst;
	COMPSTRUCT->instr_table->length++;

	return inst;
}


/* Construit une sauvegarde d'une instruction if et l'ajoute à la pile de if
 * de la structure de compilation pour être modifiée plus tard.
 */
ifCell * putIfCell(instruction * instr) {
	ifCell * ifc = malloc(sizeof(ifCell));
	ifc->instr                = instr;
	ifc->next                 = COMPSTRUCT->if_stack->top;
	COMPSTRUCT->if_stack->top = ifc;
	COMPSTRUCT->if_stack->size++;
	
	return ifc;
}

/* Supprime la sauvegarde d'instruction if en haut de la pile de if.
 * Libere l'emplacement en memoire.
 */
void popIfCell(void) {
	ifCell * memCell = COMPSTRUCT->if_stack->top;
	if (memCell != NULL) {
		COMPSTRUCT->if_stack->top = memCell->next;
		free(memCell);
	}
}

/* Récupere la sauvegarde de if en haut de la pile.
 * Cela ne la retire pas de la pile.
 */
ifCell * getIfCell(void) {
	return COMPSTRUCT->if_stack->top;
}

/* Récupere l'adresse d'instruction courante et incremente la valeur de
 * reference pour le prochaine appel.
 */
int  getAndIncInstrAdr(void) {
	int memAdr = COMPSTRUCT->instr_table->currentAdr;
	COMPSTRUCT->instr_table->currentAdr+=SIZE_OF_INSTRUCTION;
	return memAdr;
}

/* Affiche le code assembleur dans le terminal dans un format semi-textuel.
 */
void makeASM() {
	instruction * cI = COMPSTRUCT->instr_table->root;	
	printf("\n==========================\n");	
	while (cI != NULL) {
		printf("%d | %s %s, %s, %s \n", cI->address, cmdToString(cI->cmd), cI->arg1.string, cI->arg2.string, cI->arg3.string);
		cI = cI->next;
	}
	printf("==========================\n");	

}


/* transforme une instruction en code hexadecimal de format 0xXXXXXXXX
 */
void createLine(instruction * i, char* line) {	
	switch( (int)i->cmd ) {
		case ADD :
			sprintf(line, "010%s0%s0%s\n", i->arg1.string, i->arg2.string, i->arg3.string);
			break;
		case MUL :
			sprintf(line, "020%s0%s0%s\n", i->arg1.string, i->arg2.string, i->arg3.string);
			break;
		case SOU :
			sprintf(line, "030%s0%s0%s\n", i->arg1.string, i->arg2.string, i->arg3.string);
			break;
		case DIV :
			sprintf(line, "040%s0%s0%s\n", i->arg1.string, i->arg2.string, i->arg3.string);
			break;
		case COP :
			sprintf(line, "050%s0%s00\n", i->arg1.string, i->arg2.string);
			break;
		case AFC :
			sprintf(line, "060%s%s00\n", i->arg1.string, i->arg2.string);
			break;
		case LOAD :
			sprintf(line, "070%s%s00\n", i->arg1.string, i->arg2.string);
			break;
		case STORE :
			sprintf(line, "08%s0%s00\n", i->arg1.string, i->arg2.string);
			break;
		case EQU :
			sprintf(line, "090%s0%s0%s\n", i->arg1.string, i->arg2.string, i->arg3.string);
			break;
		case INF :
			sprintf(line, "0A0%s0%s0%s\n", i->arg1.string, i->arg2.string, i->arg3.string);
			break;
		case INFE :
			sprintf(line, "0B0%s0%s0%s\n", i->arg1.string, i->arg2.string, i->arg3.string);
			break;
		case SUP :
			sprintf(line, "0C0%s0%s0%s\n", i->arg1.string, i->arg2.string, i->arg3.string);
			break;
		case SUPE :
			sprintf(line, "0D0%s0%s0%s\n", i->arg1.string, i->arg2.string, i->arg3.string);
			break;
		case JMP :
			sprintf(line, "0E%s0000\n", i->arg1.string);
			break;
		case JMPC :
			sprintf(line, "0F%s0%s00\n", i->arg1.string, i->arg2.string);
			break;
		case PRINT :
			sprintf(line, "11%s0000\n", i->arg1.string);
			break;
		default :
			sprintf(line, "00000000\n");
			break;
	}
}

/* Ecrit le code assembleur compiler au format hexadecimal dans un fichier
 */
void writeASM(char* filename) {
	FILE* outFile = NULL;
	instruction * iter = NULL;	
	char* bufferLine = malloc(11*sizeof(char));

	outFile = fopen(filename, "w+");
	if (outFile == NULL) {
		exit(EXIT_FAILURE);
	}

	iter = COMPSTRUCT->instr_table->root;
	while (iter != NULL) {
		createLine(iter, bufferLine);
		fprintf(outFile, "%s", bufferLine);
		iter = iter->next;
	}

	fclose(outFile);
}
