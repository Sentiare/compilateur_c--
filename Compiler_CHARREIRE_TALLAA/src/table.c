#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "table.h"

/* trouve le symbole dans la table des symboles grace à son nom
 */
S_symbole * find(char * name) {
	S_symbole * found  = NULL;
	S_symbole * ite    = GLOBAL_TAB->root;
	bool        resume = true;
	while ((ite != NULL)&&(resume)) {
		if (strcmp(ite->name, name) == 0) {
			found = ite;
			resume = false;
		} else {
			ite = ite->next;
		}
	}
	return found;
} 

/* Ajoute un symbole à la table des symboles
 */

int put(char * n, typeEnum t, tmpEnum tmp) {
	S_symbole * s = find(n);
	S_symbole * new = malloc(sizeof(S_symbole));
	if ( (s == NULL)||((s != NULL)&&(s->depth != GLOBAL_TAB->current_depth))||(tmp == TMP) ) {
		//Creation du symbole
		new->name    = n;
		new->address = getAndIncAdr(t);
  	new->type    = t;
		new->depth   = getDepth();
		new->tmp     = tmp;

		//On accroche le tableau
		new->next    = GLOBAL_TAB->root;  

		//On accroche le nouveau symbole à la racine
		GLOBAL_TAB->root = new;
		
		//On augmente la taille du tableau
		(GLOBAL_TAB->length)++;
  }
	return new->address;	
} 

/* Initialise la table des symboles
 */
void init(void) {
	S_symbolTab * sT    = malloc(sizeof(S_symbolTab));
	sT->root            = NULL;
	sT->length          = 0;
	sT->current_depth   = 0;
	sT->current_address = REF_ADR_MEM;
	
	GLOBAL_TAB = sT;
}

/* Affiche la table des symboles dans le terminal
 */
void printTab() {
	S_symbole * ite = GLOBAL_TAB->root;
	printf("\n========================\n");
	while(ite != NULL) {
		printf("name = %s | @%d | type=%d | depth=%d | rgl=%d\n",ite->name, ite->address, ite->type, ite->depth, ite->tmp);
		ite = ite->next;
	}
	printf("========================\n");
}

/* Affiche un symbole
 */
void printSym(S_symbole * s) {
	printf("%s | %x | %d | %d\n",s->name, s->address, s->type, s->depth);
}

/* Recupere l'adresse de symbole courante et incremente la reference pour
 * les appel suivants.
 */
int getAndIncAdr(typeEnum e) {
	int save = GLOBAL_TAB->current_address;	
	switch ((int)e) {
		case INT :
			(GLOBAL_TAB->current_address)+=SIZE_OF_INT;
			break;
		case CONST :
			(GLOBAL_TAB->current_address)+=SIZE_OF_CONST;
			break;
		default :
			exit(-1);
			break;
	}
	return save;
}


/* Récupère la profondeur
 */
int getDepth() {
	return GLOBAL_TAB->current_depth;
}

/* Augmente la profondeur
 */
void incDepth() {
	(GLOBAL_TAB->current_depth)++;
}

/* Diminue la profondeur
 */ 
bool decDepth() {
  bool out         = false;
	S_symbole * memS = NULL;
	int  memAddr     = REF_ADR_MEM;
  
	//Retirer tout les symboles de profondeur courante	
	if (GLOBAL_TAB->current_depth > 0) {	
		while ((GLOBAL_TAB->root != NULL)&&(GLOBAL_TAB->root->depth == GLOBAL_TAB->current_depth)) {
			memAddr = GLOBAL_TAB->root->address;
			memS = GLOBAL_TAB->root;
			GLOBAL_TAB->root = GLOBAL_TAB->root->next;
			free(memS);
		}
		//Modification current_address;
		GLOBAL_TAB->current_address = memAddr;

		//Sortie OK
		out = true;
	}	

	//Decrease
	(GLOBAL_TAB->current_depth)--;

	//Return
	return out;
}

/* Retire tous les symboles temporaires 
 */
bool popAllTmp() {
	bool out         = false;
	S_symbole * memS = NULL;
	int memAddr      = REF_ADR_MEM;
	
	//Retirer les symboles temporaires
	if (GLOBAL_TAB->current_depth > 0) {
		while ((GLOBAL_TAB->root != NULL)&&(GLOBAL_TAB->root->tmp == TMP)) {
			memAddr = GLOBAL_TAB->root->address;
			memS = GLOBAL_TAB->root;
			GLOBAL_TAB->root = GLOBAL_TAB->root->next;
			free(memS);
		}
		//Modification current_address;
		GLOBAL_TAB->current_address = memAddr;

		//Sortie OK
		out = true;

	}

	return out;
}

/* retire et récupère le dernier symbole temporaires de la table.
 */
int popOneTMP() {
	//Def
	S_symbole * memS = NULL;
	int memAddr      = 0;

	//retirer symbole
	if ((GLOBAL_TAB->current_depth > 0)&&(GLOBAL_TAB->root->tmp == TMP)) {
		memAddr                     = GLOBAL_TAB->root->address;
		memS                        = GLOBAL_TAB->root;
		GLOBAL_TAB->root            = GLOBAL_TAB->root->next;
		GLOBAL_TAB->current_address = memAddr;
		free(memS);
	}

	//return address
	return memAddr;
}

